package com.swing.sky.system.api.tiku;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author swing
 * @since 2020-9-2
 */
@Api
@Controller
@RequestMapping("tiku/question")
public class TiQuestionErrorSubmitController {
}
